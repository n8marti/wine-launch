#!/bin/bash

# This script relies on environment variables for complete configuration.
# See https://gitlab.com/n8marti/wine-launch/-/blob/main/README.md for usage info.
WINE_LAUNCH_DIR=$(realpath "$(dirname "$0")")

# Show help if requested.
if [[ "$1" == '--help' || "$1" == '-h' ]]; then
    # USAGE=$(cat "${WINE_LAUNCH_DIR}/README.md")
    # echo "$USAGE"
    cat "${WINE_LAUNCH_DIR}/README.md"
    exit 0
elif [[ "$1" == '--kill-wine' || "$1" == '-k' ]]; then
    # Kill wine processes.
    "${WINE_LAUNCH_DIR}/scripts/kill-wine-procs.sh"
    exit 0
fi

if [[ -n "${LAUNCH_STRICT:-}" ]]; then
    set -u
fi

if [[ -n "${LAUNCH_DEBUG:-}" ]]; then
    START=$(date +%s.%N)
    set -x
fi


#####################
# Utility Functions #
#####################

# ensure_dir_exists calls `mkdir -p` if the given path is not a directory.
# This speeds up execution time by avoiding unnecessary calls to mkdir.
#
# Usage: ensure_dir_exists <path> [<mkdir-options>]...
#
ensure_dir_exists() {
    [[ -d "$1" ]] ||  mkdir -p "$@"
}

# Note: We avoid using `eval` because we don't want to expand variable names
#       in paths. For example: LD_LIBRARY_PATH paths might contain `$LIB`.
prepend_dir() {
	local -n var="$1"
	local dir="$2"
	# We can't check if the dir exists when the dir contains variables
	if [[ "$dir" == *"\$"*  || -d "$dir" ]]; then
        export "${!var}=${dir}${var:+:$var}"
	fi
}

append_dir() {
	local -n var="$1"
	local dir="$2"
	# We can't check if the dir exists when the dir contains variables
	if [[ "$dir" == *"\$"*  || -d "$dir" ]]; then
        export "${!var}=${var:+$var:}${dir}"
	fi
}

element_in() {
	local e match="$1"
	shift
	for e; do [[ "$e" == "$match" ]] && return 0; done
	return 1
}

#######################
# Sommelier functions #
#######################

init_wine() {
	echo "Initializing Wine.."

    if [[  "${WINEARCH:-}" != "win32" ]] && grep "arch=win32" "$WINEPREFIX/system.reg" >/dev/null 2>&1 ; then
        echo "WARNING: Old Wine prefix is win32, but WINEARCH is not 'win32'. Backing up old Wine prefix and generating a win64 one."
        mv "$WINEPREFIX" "${WINEPREFIX}.bak-$(date +%s)"
	fi

	# Create the Wineprefix
	"${WINELOADER}" wineboot --init 2>&1 #| \
        # yad --progress --title="Preparing Windows compatibility layer" \
        # --progress-text= --width=400 --center --no-buttons --auto-close --auto-kill --pulsate

	# Opt out of winetricks metrics - we ninja this to avoid dialogs
	# if [[ ! -f "${XDG_CACHE_HOME}/winetricks/track_usage" ]]; then
    #     mkdir -p "${XDG_CACHE_HOME}/winetricks"
    #     echo 0 > "${XDG_CACHE_HOME}/winetricks/track_usage"
	# fi

	# link ttf & ttc fonts from root to wineprefix
	# ensure_dir_exists "$WINEPREFIX/drive_c/windows/Fonts"
	# find /usr/share/fonts/ -type f \( -name "*.ttf" -o -name "*.ttc" \) -exec ln -vs "{}" "$WINEPREFIX/drive_c/windows/Fonts/" \; &>/dev/null

	# if [[ -z "${SOMMELIER_NO_THEME:-}" ]]; then
    #     # Install Zhiyi's light theme and set as default
    #     ensure_dir_exists "$WINEPREFIX/drive_c/windows/Resources/Themes/light"
    #     cp -r "$SNAP/sommelier/themes/"* "$WINEPREFIX/drive_c/windows/Resources/Themes"
    #     "$WINELOADER" regedit "$SNAP/sommelier/themes/light/light.reg"
    #     sleep 1
	# fi

    # Run post-init hook.
    if [[ -f "${HOOKS_DIR}/post-init" ]]; then
        echo "Running hook '${HOOKS_DIR}/post-init'"
        . "${HOOKS_DIR}/post-init"
    fi
}

install_app() {
	echo "Installing application..."

	ORIGINAL_LINKS=( $(xdg-user-dir DESKTOP)/*.lnk )

	# Install additional requirements via winetricks.
	if [[ -n "${TRICKS:-}" ]]; then
        for TRICK in ${TRICKS}; do
            echo "Running 'winetricks ${TRICK}'..."
            "${WINETRICKS}" --unattended "${TRICK}" #| \
                # yad --progress --title="Installing ${TRICK}" --progress-text= \
                # --width=400 --center --no-buttons --auto-close --auto-kill --on-top --pulsate
            if [[ $? -ne 0 ]]; then
                echo "ERROR: 'winetricks $TRICK' failed."
                exit 1
            fi
        done
	fi

    # Run pre-install hook.
	if [[ -f "${HOOKS_DIR}/pre-install" ]]; then
        echo "Running hook '${HOOKS_DIR}/pre-install'"
        . "${HOOKS_DIR}/pre-install"
	fi

	# Download installer if requested.
	if [[ -n "${INSTALL_URL:-}" ]]; then
        # If we've been given an installer URL derive the filename
        INSTALL_EXE="$(basename "${INSTALL_URL//\?*/}")"

        if [[ -e "${TMPDIR}/${INSTALL_EXE}" ]]; then
            LOCAL_INSTALL_EXE="${TMPDIR}/${INSTALL_EXE}"
        else
            wget "${INSTALL_URL}" -O "${TMPDIR}/${INSTALL_EXE}" 2>&1 #| \
                # perl -p -e '$| = 1; s/^.* +([0-9]+%) +([0-9,.]+[GMKB]) +([0-9hms,.]+).*$/\1\n# Downloading... \2 (\3)/' | \
                # yad --progress --title="Downloading ${INSTALL_EXE}" --width=400 --center \
                # --no-buttons --auto-close --auto-kill --on-top --no-escape

            LOCAL_INSTALL_EXE="${TMPDIR}/${INSTALL_EXE}"
        fi
	elif [[ -n "${INSTALL_EXE:-}" ]]; then
        LOCAL_INSTALL_EXE="${INSTALL_EXE}"
	fi

	# Install the application in Wine.
	if [[ -n "${LOCAL_INSTALL_EXE:-}" ]]; then
        # Convert INSTALL_FLAGS to an array to support multiple quoted arguments.
        # https://stackoverflow.com/a/37381300/1588555
        IFS=$'\n' ARR_INSTALL_FLAGS=( $(xargs -n1 <<<"${INSTALL_FLAGS:-}") )
        # Piping the wine command to yad gives weird behavior, so instead we use
        # yad in the background and kill it after the installation finishes.
        EXE_NAME=$(basename "${LOCAL_INSTALL_EXE}")
        # yes | yad --progress --title="Installing ${EXE_NAME}" --progress-text= --width=400 --center --no-buttons --auto-close --auto-kill --on-top --pulsate &
        # INSTALL_YAD_PID="$!"
        FILE_END="${EXE_NAME##*.}"
        FILE_TYPE="${FILE_END,,}" # force lowercase
        if [[ "$FILE_TYPE" == 'exe' ]]; then
            "${WINELOADER}" "${LOCAL_INSTALL_EXE}" "${ARR_INSTALL_FLAGS[@]}"
        elif [[ "$FILE_TYPE" == 'msi' ]]; then
            "${WINELOADER}" msiexec /i "${LOCAL_INSTALL_EXE}" "${ARR_INSTALL_FLAGS[@]}"
        elif [[ "$FILE_TYPE" == 'zip' ]]; then
            out_dir='/tmp/wine-launch'
            inst_dir=$(dirname "$RUN_EXE")
            mkdir -p "$out_dir" "$inst_dir"
            unzip -d "$out_dir" "${LOCAL_INSTALL_EXE}"
            cp -r "${out_dir}/"* "$inst_dir"
            rm -rf "$out_dir"
        else
            echo "WARNING: Unsupported file type: $FILE_TYPE. This feature is deprecated, please contact the sommelier-core devs for more info."
            "${WINELOADER}" "${LOCAL_INSTALL_EXE}" "${ARR_INSTALL_FLAGS[@]}"
        fi
        # kill "$INSTALL_YAD_PID"
	fi

	# Remove the cached installer.
	# if [[ -n "${INSTALL_URL:-}" ]]; then
    #     rm -v "${TMPDIR}/${INSTALL_EXE}"
	# fi

	# Run post-install hook.
	if [[ -f "${HOOKS_DIR}/post-install" ]]; then
        echo "Running hook '${HOOKS_DIR}/post-install'"
        . "${HOOKS_DIR}/post-install"
	fi

	# Remove any Windows shortcuts the installer created since these don't work
	# in Linux and some users get confused by them.
	for CURRENT_LINK in $(xdg-user-dir DESKTOP)/*.lnk; do
        element_in "$CURRENT_LINK" "${ORIGINAL_LINKS[@]}" || rm "$CURRENT_LINK"
	done
}

start_app() {
	echo "Starting application..."

	if [[ -f "${HOOKS_DIR}/pre-start" ]]; then
        echo "Running hook '${HOOKS_DIR}/pre-start'"
        . "${HOOKS_DIR}/pre-start"
	fi

	# Convert all arguments which point to existing files/dirs to Windows paths.
	for i in "${ACTION_ARGS[@]}"; do
        if [[ -e "${i}" ]]; then
            args+=( "$(winepath -w "$i")" )
        else
            args+=( "$i" )
        fi
	done
	ACTION_ARGS=( "${args[@]}" )

    if [[ -n "${LAUNCH_DEBUG:-}" ]]; then
        echo "DEBUG: Sommelier elapsed time: $(date +%s.%N --date="$START seconds ago")"
        echo "DEBUG: Now running: '${WINELOADER}' '${RUN_EXE}' '${ACTION_ARGS[@]}'"
	fi

	# Because Windows software will often look for program files in the location
	# they were started from we need to start it in a very specific way: change
	# directory to the folder where the program is located and run the .exe file
	# using _only_ its filename.
    if [[ -z "${LAUNCH_KEEP_CWD:-}" ]]; then
        app_dir=$(dirname "${RUN_EXE}")
        app=$(basename "${RUN_EXE}")
        cd "${app_dir}"
	else
        app="${RUN_EXE}"
	fi

	# Note 1: We use `wine` because `wine start \wait \unix` has problems with
	#         relative unix-style paths.
	# Note 2: `wine` already waits until the program exits, so we don't need to
	#          manually wait here.
	# Note 3: If requested, we run the app in a new virtual desktop. This can
	#         elimitate bugs with alt-tab and fullscreen on XWayland.
    if [[ -n "${LAUNCH_VIRTDESKTOP:-}" ]]; then
        local native_res
        if [[ "${XDG_SESSION_TYPE}" == "x11" ]]; then
            native_res=$(xrandr -q | grep primary | cut -d' ' -f4 | cut -d'+' -f1)
        else
            # Wayland doesn't have a "primary display". We use the biggest screen
            # instead.
            native_res=$(xrandr -q | grep XWAYLAND | cut -d' ' -f3 | cut -d'+' -f1 | sort -r -n | sort -r -n | head -n 1)
        fi
        "${WINELOADER}" explorer /desktop="$app","${native_res}" "${app}" "${ACTION_ARGS[@]}"
        local exit_code="$?"
	else
        "${WINELOADER}" "${app}" "${ACTION_ARGS[@]}"
        local exit_code="$?"
	fi

	if [[ -f "${HOOKS_DIR}/post-stop" ]]; then
        echo "Running hook '${HOOKS_DIR}/post-stop'"
        . "${HOOKS_DIR}/post-stop"
	fi

	exit $exit_code
}


# Set default variables.
# Note: Technically, we should be able to just use the `wine` command and get
#       Wine location from PATH, but it doesn't hurt to use the absolute path.
# Note: The `wine` command runs 64-bit Wine by default, unless specified
#       otherwise by WINEARCH
# Note: We don't need to manually specify WINEDLLPATH, `wine` can find them on
#       its own.

# APP_SHARE_DIR has to be set before setup hook so that HOOKS_DIR can be found,
#   so it either needs to be set previously in the terminal or allowed to
#   default to PWD.
if [[ -z "${APP_SHARE_DIR}" ]]; then
    export APP_SHARE_DIR="$PWD"
fi

# Set HOOKS_DIR.
if [[ ! -d "${APP_SHARE_DIR}/hooks" ]]; then
    echo "ERROR: No 'hooks' folder found in ${APP_SHARE_DIR}."
    echo -e "\nNeed help? Try:\n$0 --help"
    exit 1
else
    HOOKS_DIR="${APP_SHARE_DIR}/hooks"
fi

if [[ -z "${WINEPREFIX:-}" ]]; then
    export WINEPREFIX="${APP_SHARE_DIR}/wine-prefix"
fi

# Setup hook is useful for:
#   - setting environment variables; e.g.
#       export WINEARCH=win32
#       export WINEPREFIX="${HOME}/.local/share/myapp/wine"
#       export RUN_EXE="${WINEPREFIX}/drive_c/Program Files/MyApp/MyApp.exe"
#   - installing wine or other package(s)
#       sudo dpkg --add-architecture i386
#       sudo apt-get update
#       sudo apt-get install -y wine-stable
# Run setup hook.
if [[ -f "${HOOKS_DIR}/setup" ]]; then
    echo "Running hook '${HOOKS_DIR}/setup'"
    . "${HOOKS_DIR}/setup"
fi

if [[ -z "${WINELOADER:-}" ]]; then
    export WINELOADER=$(readlink -f "$(which wine)")
fi

if [[ -z "${WINE:-}" ]]; then
    export WINE="$WINELOADER" # used by winetricks
fi

if [[ -z "${WINEDEBUG:-}" ]]; then
	export WINEDEBUG="-all"
fi

if [[ -z "${WINETRICKS:-}" ]]; then
    export WINETRICKS=$(readlink -f "$(which winetricks)")
fi


###########################
# Wine platform selection #
###########################

#WINE_RELEASE=$(readlink -f $(which wine)) | sed -E 's@/.*(wine.*)/bin/wine@\1@'

# append_dir LD_LIBRARY_PATH "$WINE_PLATFORM/lib"
# append_dir LD_LIBRARY_PATH "$WINE_PLATFORM/lib64"


###########################
# Wine runtime selection #
###########################

export DXVK_CONFIG_FILE="$WINEPREFIX/dxvk.conf"
if [[ -z "${DXVK_LOG_LEVEL:-}" ]]; then
	export DXVK_LOG_LEVEL="none"
fi

# append_dir LD_LIBRARY_PATH "$SNAP/lib"
# append_dir LD_LIBRARY_PATH "$SNAP/\$LIB"
# append_dir LD_LIBRARY_PATH "$SNAP/usr/lib"
# append_dir LD_LIBRARY_PATH "$SNAP/usr/\$LIB"

# Don't Stop wine from updating $WINEPREFIX automatically.
if [[ -f "${WINEPREFIX}/.update-timestamp" ]]; then
	chkstmp="$(grep -c disable "${WINEPREFIX}/.update-timestamp")"
	if [[ "$chkstmp" -eq 1 ]]; then
    rm "${WINEPREFIX}/.update-timestamp"
	fi
fi

# Disable the WineMenuBuilder. It creates Desktop shortcuts and configures
# file associations for the applications you install in wine. This does not
# work in a snap, however, since those are handled by snapd instead. The only
# thing it does is pollute ~/Desktop with broken `.desktop` files.
# Commented out b/c works fine when used outside of snapd.
# export WINEDLLOVERRIDES="${WINEDLLOVERRIDES:-};winemenubuilder.exe=d"

if [[ -n "${LAUNCH_DEBUG:-}" ]]; then
    echo "wine-launch environment variables:"
    printenv | grep -e RUN_EXE -e INSTALL -e LAUNCH -e WINE | sort
fi

if [[ ! -f "${WINEPREFIX}/drive_c/windows/system.ini" ]]; then
	init_wine
fi


#####################################
# Actually run the requested action #
#####################################

# Get the action we're asked to run.
if [[ -n "${1:-}" ]]; then
	ACTION="${1}"
	shift
	ACTION_ARGS=( "$@" )
else
	# echo "ERROR: I need an action as first argument. Exiting here."
	# exit 1
    ACTION="run-exe"
fi

# Start the requested program.
if [[ "$ACTION" = "winetricks" ]]; then
	"$WINETRICKS" "${ACTION_ARGS[@]}"
elif [[ "$ACTION" = "winedbg" ]]; then
	"$ACTION" "${ACTION_ARGS[@]}"
elif [[ "$ACTION" = "run-exe" ]]; then
	# Check if we know which executable to run.
	# Note: RUN_EXE is provided as an environment variable instead of a CLI
	# parameter so that snaps using command-chain can specify an executable
	# with a space in the path without having to use a wrapper script.
	if [[ -z "${RUN_EXE:-}" ]]; then
        echo "ERROR: No executable provided. Please provide the path to the executable using the RUN_EXE environment variable."
        exit 1
	fi

	# Convert Windows path to unix
	RUN_EXE=$("$WINELOADER" winepath "$RUN_EXE")

	# Install if the executable doesn't exist or the snap version changed.
	if [[ ! -e "$RUN_EXE" ]]; then
        install_app
        # If installer is interactive, this script won't recognize that. So need
        #   to check again for RUN_EXE, then exit if not found so that user can
        #   finish installation then run again.
    	if [[ ! -e "$RUN_EXE" ]]; then
            echo "WARNING: '$RUN_EXE' not installed. If installer is interactive and still open, run $(realpath $0) again when installation is complete."
            exit 1
        fi
	fi

	# Run the application
	start_app
else
	"$WINELOADER" "$ACTION" "${ACTION_ARGS[@]}"
fi
