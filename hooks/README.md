### Hooks
Hooks are bash scripts that are sourced by ../launch-app.sh to aid in proper configuration and execution of the WINE system. Only the "setup" hook is necessary, the others are optional.

### Valid hooks include:
- setup (required if environment variables are not set elsewhere)
- post-init
- pre-install
- post-install
- pre-start
- post-stop
