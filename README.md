# Wine Launch

Generic script to install and run a Windows executable via WINE.

## Usage

This script relies on environment variables for complete configuration.  
**Hint: Either define these in the shell before running the script, or put them in a hooks/setup script, which will be sourced by launch-app.sh.**

### Required environment variables:
- RUN_EXE (path to installed EXE file in wine prefix)
- *Either:*
  - INSTALL_EXE (local path to EXE/MSI installer); or
  - INSTALL_URL (url path to EXE/MSI installer)

### Allowed environment variables:
- APP_SHARE_DIR (path to app's WINE prefix and 'hooks' folders; defaults to PWD)
- WINEPREFIX (wine prefix folder, defaults to '$APP_SHARE_DIR/wine-prefix')
- INSTALL_FLAGS (added to the installer command)
- LAUNCH_DEBUG (show debug output for launch-app.sh)
- LAUNCH_KEEP_CWD (don't cd into parent of RUN_EXE before execution)
- LAUNCH_STRICT (exit with error if an undefined variable is used)
- LAUNCH_VIRTDESKTOP (force wine app to use its own desktop; e.g. if it requires fullscreen)
- TRICKS (space-separated list of winetricks to install)
- *Typical WINE variables, including:*
  - WINEARCH (defaults to 64-bit if unset)
  - WINEDEBUG
  - WINEDLLOVERRIDES
  - WINELOADER (wine executable; defaults to "wine" in PATH)
  - WINETRICKS (winetricks executable; defaults to "winetricks" in PATH)

## Acknowledgment
Heavily based on [sommelier-core](https://gitlab.com/sommelier/sommelier-core/) but tweaked for use with deb-based packages instead of snaps.
