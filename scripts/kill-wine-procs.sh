#!/bin/bash

# Kill WINE processes. Could use '.exe', but that risks unintended consequences.
exe_procs_to_kill='conhost.exe|dfsvc.exe|explorer.exe|mscorsvw.exe|plugplay.exe|rpcss.exe|services.exe|svchost.exe|winedevice.exe'
if [[ -z $(pgrep -a "$exe_procs_to_kill") ]]; then
    exit 0
fi

while read -r line; do
    echo "killing pid: ${line}"
    pid=$(echo "$line" | awk '{print $1}')
    kill $pid
done <<< $(pgrep -a "$exe_procs_to_kill")
