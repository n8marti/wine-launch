## Apps that depend on .NET <5 and should be tried via WINE
- [Cog](https://software.sil.org/cog/download/)
  - runs; needs testing
- [Glyssen](https://software.sil.org/glyssen/download/)
- [HearThis](https://software.sil.org/hearthis/download/)
  - runs; needs testing
- [LIFT Tools](https://software.sil.org/lifttools/) (.NET 3.5 SP1)
- [Phonology Assistant](https://software.sil.org/phonologyassistant/download)
- [SayMore](https://software.sil.org/saymore/download/)
  - runs, but has fatal error when creating a new session using "New..."
  - works with manually-created test session folder & data, followed by user opening & closing test project before any other
  - other info in repo: https://github.com/wasta-linux/saymore-snap

## Other apps with Windows-only installers available
- [Cardsorter](http://casali.canil.ca/CardSorter/)
- [Dekereke](https://casali.canil.ca/Dekereke.zip)

## Reference links
- https://software.sil.org/software-products/
- http://lingtransoft.info/apps/results?page=0